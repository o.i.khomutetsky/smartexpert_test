<?php

namespace App\Http\Controllers;

use App\Link;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $links = Link::where('user_id', Auth::user()->id)->where('due', '>', Carbon::now())->get();
        return view('index', compact("links"));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function saveLink(Request $request)
    {
        $link = Link::create($request->all());
        $link->short_link = $this->getLink();
        $link->user_id = Auth::user()->id;
        $link->due = Carbon::now()->addDays($request->due);
        $link->save();
        $links = Link::where('user_id', Auth::user()->id)->where('due', '>', Carbon::now())->get();
        return view('index', compact("links"));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function link(Request $request)
    {
        $link_info = Link::where('short_link', $request->link)->first();
        return redirect($link_info->original_link);
    }

    /**
     * @return string
     */
    private function getLink(){
        $link = Str::random(4);
        $count = Link::where('short_link', $link)->count();
        if($count)
            $link = $this->getLink();
        return $link;
    }
}
