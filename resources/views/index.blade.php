@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add Link</div>

                <div class="card-body">
                        <form method="post">
                            @csrf
                            <div class="form-group">
                                <label for="original_link">Original link</label>
                                <input type="text" name='original_link' class="form-control" id="original_link" placeholder="Enter link">
                            </div>
                            <div class="form-group">
                                <label for="due">Lifetime</label>
                                <select name='due' id="due" class="form-control">
                                    <option value="1">1 day</option>
                                    <option value="7">1 week</option>
                                    <option value="30">1 month</option>
                                    <option value="365">1 year</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            @forelse($links as $link)
                <p><a href="{{ $link->original_link }}">{{ url($link->short_link) }}</a></p>
            @empty
                <p>There are no added links yet=(</p>
            @endforelse
        </div>
    </div>
</div>
@endsection
